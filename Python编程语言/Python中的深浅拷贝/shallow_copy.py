"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import copy

old = [[1, 2], [3, 4]]

# 浅拷贝，得到副本
new = copy.copy(old)
print("Old list:", old)  # Old : [[1, 2], [3, 4]]
print("New list:", new)  # New : [[1, 2], [3, 4]]

# 给原始对象添加新元素
old.append([5, 6])
print("Old :", old)  # Old : [[1, 2], [3, 4], [5, 6]]
print("New :", new)  # New : [[1, 2], [3, 4]]

# 修改原始对象
old[1][1] = 'hogwarts'
print("Old :", old)  # Old : [[1, 2], [3, 'hogwarts'], [5, 6]]
print("New :", new)  # New : [[1, 2], [3, 'hogwarts']]
