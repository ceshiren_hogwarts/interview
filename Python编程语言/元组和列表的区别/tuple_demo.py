

# 创建元组对象
tup = ("python", "hogwarts", 1, 2, 3)

# 元组对象不可改变
tup[0] = "hello"  # 引发 TypeError