
import sys


def test_request_fixture(request):
    print(f'发现的测试用例: {request.session.testscollected}')
    print(f'失败的测试用例: {request.session.testsfailed}')


def test_capsys_fixture_stdout(capsys):
    print("Hogwarts", end="")
    captured = capsys.readouterr()
    assert "Hogwarts" == captured.out


def test_capsys_fixture_stderr(capsys):
    sys.stderr.write("ERROR")
    captured = capsys.readouterr()
    assert "ERROR" == captured.err


def test_tempdir_fixture(tmpdir):
    print(f"临时目录: {tmpdir}")