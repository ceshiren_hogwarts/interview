class PowTwo:
    """2的指数迭代器"""

    def __init__(self, max=0):
        self.max = max

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n <= self.max:
            result = 2 ** self.n
            self.n += 1
            return result
        else:
            raise StopIteration


numbers = PowTwo(3)  # 实例化
i = iter(numbers)  # 获得一个迭代器
print(next(i))  # 获取下一个元素
print(next(i))  # 获取下一个元素
print(next(i))  # 获取下一个元素
print(next(i))  # 获取下一个元素
print(next(i))  # 获取下一个元素