def my_gen():
    n = 1
    # 生成器函数包含yield语句
    yield n

    n += 1
    yield n

    n += 1
    yield n


# 使用for循环遍历
for item in my_gen():
    print(item)